import { Injectable } from '@nestjs/common';

@Injectable()
export class HashtagService {
  create() {
    return 'This action adds a new hashtag';
  }

  findAll() {
    return `This action returns all hashtag`;
  }

  findOne(id: number) {
    return `This action returns a #${id} hashtag`;
  }

  update(id: number) {
    return `This action updates a #${id} hashtag`;
  }

  remove(id: number) {
    return `This action removes a #${id} hashtag`;
  }
}
